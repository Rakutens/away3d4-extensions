package primitives {
	import away3d.entities.Mesh;
	import away3d.materials.TextureMaterial;
	import away3d.textures.BitmapTexture;
	
	import flash.events.Event;
	import flash.utils.Dictionary;
	
	import net.redefy.away3d.primitives.GemstoneGeometry;
	
	/**
	 * Тестируем драгоценный камень.
	 * @author redefy
	 */
	public class TestGemstone extends BaseTest {
		[Embed(source = '/resources/textures/texture21.jpg')] public const Texture0:Class;
		
		public var geometry:GemstoneGeometry;
		private var gemstone:Mesh;
		private var property:Dictionary;
		
		/** Конструктор. */
		public function TestGemstone() {
			super();
			initGUI();
		}
		
		/** Возвращает камеру в позицию по умолчанию. */
		public function resetCamera():void {
			controller.panAngle = 0;
			controller.tiltAngle = 10;
			controller.distance = 1000;
		}
		
		/** Возвращает меш на исходное состояние. */
		public function resetMesh():void {
			property["segments"] 		= geometry.segments = 24;
			property["pavilionRadius"] 	= geometry.pavilionRadius = 300;
			property["crownRadius"] 	= geometry.crownRadius = 150;
			property["crownHeight"] 	= geometry.crownHeight = 100;
			property["pavilionHeight"] 	= geometry.pavilionHeight = 200;
			gemstone.geometry = geometry;
		}
		
		/** @inheritDoc */
		override protected function initObjects():void {
			var material:TextureMaterial = new TextureMaterial(new BitmapTexture(new Texture0().bitmapData));
			geometry = new GemstoneGeometry(24, 300, 150, 100, 200);
			gemstone = new Mesh(geometry, material);
			conteiner.addChild(gemstone);
			
			property = new Dictionary();
			property["segments"] 		= geometry.segments;
			property["pavilionRadius"] 	= geometry.pavilionRadius;
			property["crownRadius"] 	= geometry.crownRadius;
			property["crownHeight"] 	= geometry.crownHeight;
			property["pavilionHeight"] 	= geometry.pavilionHeight;
		}
		
		/** @inheritDoc */
		override protected function update(e:Event):void {
			for (var p:String in property) { 
				if (property[p] != geometry[p]) {
					property[p] = geometry[p];
					gemstone.geometry = geometry;
					return;
				}
			}
			super.update(e);
		}
		
		/** Инициализация GUI. */
		private function initGUI():void {
			var _gui:SimpleGUI = new SimpleGUI(this, "TEST GEMSTONE", "C");
			
			_gui.addButton("Reset Camera", { callback:resetCamera,  width:150 } );
			_gui.addButton("Reset Mesh",   { callback:resetMesh,    width:150 } );
			
			_gui.addColumn("Gemstone Properties");
				
			_gui.addSlider("geometry.segments", 0, 100, { label:"Segments", tick:1 } );
			_gui.addSlider("geometry.pavilionRadius", 0, 400, { label:"PavilionRadius", tick:0.1 } );
			_gui.addSlider("geometry.crownRadius", 0, 400, { label:"CrownRadius", tick:0.1 } );
			_gui.addSlider("geometry.crownHeight", 0, 200, { label:"CrownHeight", tick:0.1 } );
			_gui.addSlider("geometry.pavilionHeight", 0, 400, { label:"PavilionHeight", tick:0.1 } );
			_gui.show();
		}
	}
}