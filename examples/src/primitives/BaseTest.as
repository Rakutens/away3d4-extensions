package primitives {
	import away3d.cameras.Camera3D;
	import away3d.containers.ObjectContainer3D;
	import away3d.containers.Scene3D;
	import away3d.containers.View3D;
	import away3d.controllers.HoverController;
	import away3d.debug.AwayStats;
	import away3d.textures.BitmapTexture;
	
	import flash.display.*;
	import flash.events.*;
	import flash.ui.*;
	
	/**
	 * Базовый класс.
	 * @author redefy
	 */
	public class BaseTest extends Sprite {
		[Embed(source='/resources/textures/back.jpg')]	public const TextureBack:Class;
		
		protected var camera:Camera3D; 				
		protected var scene:Scene3D; 				
		protected var view:View3D; 					
		protected var conteiner:ObjectContainer3D; 	
		protected var controller:HoverController; 	
		
		private var move:Boolean = false; 			
		private var lastPanAngle:Number; 			
		private var lastTiltAngle:Number; 			
		private var lastMouseX:Number; 				
		private var lastMouseY:Number; 				
		protected var tiltSpeed:Number = 5; 		
		protected var panSpeed:Number = 5; 			
		protected var distanceSpeed:Number = 15; 	
		private var tiltIncrement:Number = 0; 		
		private var panIncrement:Number = 0; 		
		private var distanceIncrement:Number = 0; 
		
		/** Конструктор. */
		public function BaseTest() {
			initScene();
			initController();
			initObjects();
			addListeners();
		}
		
		/** Инициализация сцены. */
		protected function initScene():void {
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			
			camera = new Camera3D();
			camera.lens.near = 10;
			camera.lens.far = 10000;
			
			scene = new Scene3D();
			
			conteiner = new ObjectContainer3D();
			scene.addChild(conteiner);
			
			view = new View3D(scene, camera);
			view.antiAlias = 4;
			view.background = new BitmapTexture(new TextureBack().bitmapData);
			addChild(view);
			
			var stats:AwayStats = new AwayStats(view, false, false, 5);
			stats.x = stage.stageWidth - 125;
			addChild(stats);
		}
		
		/** Инициализация контроллера. */
		protected function initController():void {
			controller = new HoverController(camera, conteiner, 0, 10, 1000, -90, 90);
			controller.yFactor = 1.4;
		}
		
		/** Инициализация объектов. */
		protected function initObjects():void {
			//здесь будут создаваться объекты и добавляться в контейнер
		}
		
		/** Добавление слушетелей. */
		protected function addListeners():void {
			addEventListener(Event.ENTER_FRAME, update);
			view.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			view.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
			stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
			stage.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
		}
		
		/** Обновление сцены. */
		protected function update(e:Event):void {
			if (move) {
				controller.panAngle = 0.3 * (stage.mouseX - lastMouseX) + lastPanAngle;
				controller.tiltAngle = 0.3 * (stage.mouseY - lastMouseY) + lastTiltAngle;
			}
			controller.panAngle += panIncrement;
			controller.tiltAngle += tiltIncrement;
			controller.distance += distanceIncrement;
			
			view.render();
		}
		
		/** Вызывается при нажатии клавиши клавиатуры. */
		private function onKeyDown(event:KeyboardEvent):void {
			switch (event.keyCode) {
				case Keyboard.UP: 
				case Keyboard.W: 
					tiltIncrement = tiltSpeed;
					break;
				case Keyboard.DOWN: 
				case Keyboard.S: 
					tiltIncrement = -tiltSpeed;
					break;
				case Keyboard.LEFT: 
				case Keyboard.A: 
					panIncrement = -panSpeed;
					break;
				case Keyboard.RIGHT: 
				case Keyboard.D: 
					panIncrement = panSpeed;
					break;
				case Keyboard.Z: 
					distanceIncrement = distanceSpeed;
					break;
				case Keyboard.X: 
					distanceIncrement = -distanceSpeed;
					break;
			}
		}
		
		/** Вызывается при отпускании клавиши клавиатуры. */
		private function onKeyUp(event:KeyboardEvent):void {
			switch (event.keyCode) {
				case Keyboard.UP: 
				case Keyboard.W: 
				case Keyboard.DOWN: 
				case Keyboard.S: 
					tiltIncrement = 0;
					break;
				case Keyboard.LEFT: 
				case Keyboard.A: 
				case Keyboard.RIGHT: 
				case Keyboard.D: 
					panIncrement = 0;
					break;
				case Keyboard.Z: 
				case Keyboard.X: 
					distanceIncrement = 0;
					break;
			}
		}
		
		/** Вызывается при нажатии левой кнопки мыши по вьюпорту. */
		private function onMouseDown(event:MouseEvent):void {
			move = true;
			lastPanAngle = controller.panAngle;
			lastTiltAngle = controller.tiltAngle;
			lastMouseX = stage.mouseX;
			lastMouseY = stage.mouseY;
			stage.addEventListener(Event.MOUSE_LEAVE, onStageMouseLeave);
		}
		
		/** Вызывается при отпускании левой кнопки мыши. */
		private function onMouseUp(event:MouseEvent):void {
			move = false;
			stage.removeEventListener(Event.MOUSE_LEAVE, onStageMouseLeave);
		}
		
		private function onStageMouseLeave(event:Event):void {
			move = false;
			stage.removeEventListener(Event.MOUSE_LEAVE, onStageMouseLeave);
		}
	}
}