package primitives {
	import away3d.entities.Mesh;
	import away3d.materials.TextureMaterial;
	import away3d.textures.BitmapTexture;
	
	import flash.events.Event;
	import flash.utils.Dictionary;
	
	import net.redefy.away3d.primitives.DiamondGeometry;
	
	/**
	 * Тестируем алмаз.
	 * @author redefy
	 */
	public class TestDiamond extends BaseTest {
		[Embed(source = '/resources/textures/texture22.jpg')] public const Texture0:Class;
		
		public var geometry:DiamondGeometry;
		private var diamond:Mesh;
		private var property:Dictionary;
		
		/** Конструктор. */
		public function TestDiamond() {
			super();
			initGUI();
		}
		
		/** Возвращает камеру в позицию по умолчанию. */
		public function resetCamera():void {
			controller.panAngle = 0;
			controller.tiltAngle = 10;
			controller.distance = 1000;
		}
		
		/** Возвращает меш на исходное состояние. */
		public function resetMesh():void {
			property["segments"] 		= geometry.segments = 32;
			property["girdleRadius"] 	= geometry.girdleRadius = 300;
			property["tableRadius"] 	= geometry.tableRadius = 150;
			property["crownHeight"] 	= geometry.crownHeight = 100;
			property["pavilionHeight"] 	= geometry.pavilionHeight = 200;
			diamond.geometry = geometry;
		}
		
		/** @inheritDoc */
		override protected function initObjects():void {
			var material:TextureMaterial = new TextureMaterial(new BitmapTexture(new Texture0().bitmapData));
			geometry = new DiamondGeometry(32, 300, 150, 100, 200);
			diamond = new Mesh(geometry, material);
			conteiner.addChild(diamond);
			
			property = new Dictionary();
			property["segments"] 		= geometry.segments;
			property["girdleRadius"] 	= geometry.girdleRadius;
			property["tableRadius"] 	= geometry.tableRadius;
			property["crownHeight"] 	= geometry.crownHeight;
			property["pavilionHeight"] 	= geometry.pavilionHeight;
		}
		
		/** @inheritDoc */
		override protected function update(e:Event):void {
			for (var p:String in property) { 
				if (property[p] != geometry[p]) {
					property[p] = geometry[p];
					diamond.geometry = geometry;
					return;
				}
			}
			super.update(e);
		}
		
		/** Инициализация GUI. */
		private function initGUI():void {
			var _gui:SimpleGUI = new SimpleGUI(this, "TEST DIAMOND", "C");
			
			_gui.addButton("Reset Camera", { callback:resetCamera,  width:150 } );
			_gui.addButton("Reset Mesh",   { callback:resetMesh,    width:150 } );
			
			_gui.addColumn("Diamond Properties");
				
			_gui.addSlider("geometry.segments", 0, 100, { label:"Segments", tick:1 } );
			_gui.addSlider("geometry.girdleRadius", 0, 400, { label:"GirdleRadius", tick:0.1 } );
			_gui.addSlider("geometry.tableRadius", 0, 400, { label:"TableRadius", tick:0.1 } );
			_gui.addSlider("geometry.crownHeight", 0, 200, { label:"CrownHeight", tick:0.1 } );
			_gui.addSlider("geometry.pavilionHeight", 0, 400, { label:"PavilionHeight", tick:0.1 } );
			_gui.show();
		}
	}
}